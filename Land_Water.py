#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 13:09:15 2022

@author: Sophie
"""

import numpy as np 
import matplotlib.pyplot as plt

dtype = np.uint8

try:
    worldmap = np.fromfile("gl-latlong-1km-landcover.bsq",dtype)
except IOError:
    print('Error While Opening the file!')    

worldmap = np.reshape(worldmap, (21600, 43200))

#plt.imshow(worldmap[::50,::50])
#plt.show()

#x_direction = W or E
#y_direction = N or S

def map_locater(x_coordinate, x_direction, y_coordinate, y_direction):
    x_pixel=0.0
    y_pixel=0.0
    
    if x_direction == "W":
        x_pixel = 21600.0-(x_coordinate/(360.0/43200.0))
    else:
        x_pixel = 21600.0+(x_coordinate/(360.0/43200.0))
        
    if y_direction == "S":
        y_pixel = 10800.0+(y_coordinate/(180.0/21600.0))
    else:
        y_pixel = 10800.0-(y_coordinate/(180.0/21600.0))
    
    return x_pixel, y_pixel

x_pixel, y_pixel = map_locater(13.4050, "E", 52.5200, "N")

#print(int(x_pixel), int(y_pixel))
#print(worldmap[int(y_pixel)][int(x_pixel)])

land_type = {0 : 'Water',
             1 : 'Evergreen needleleaf forest',
             2 : 'Evergreen broadleaf forest',
             3 : 'Deciduous needleleaf forest',
             4 : 'Deciduous broadleaf forest',
             5 : 'Mixed Forest',
             6 : 'Woodland', 
             7 : 'Wooded grassland',
             8 : 'Closed Shrubland',
             9 : 'open Shrubland', 
             10: 'Grassland',
             11: 'Cropland',
             12: 'Bare ground',
             13: 'Urban'}

print(land_type[worldmap[int(y_pixel)][int(x_pixel)]])